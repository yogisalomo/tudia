package com.example.tudia;

import java.io.File;

//import com.example.cam3.DrawActivity;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class Cam extends Activity {

	private File mPictureFile;
	public static final String TMP_FILE_NAME = ".cam";
	private static final int CAMERA_REQUEST_CODE = 100;
	public static final String PICTURE_FILE_ID = "picture";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		takePicture();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private File getFile() {
		return new File(getExternalFilesDir(null), TMP_FILE_NAME);
	}
	
	private void takePicture() {
		mPictureFile = getFile();
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mPictureFile));
		startActivityForResult(intent, CAMERA_REQUEST_CODE);
	}
	
	private void draw() {
		if (mPictureFile != null) {
			Intent intent = new Intent(this, DrawActivity.class);
			intent.putExtra(PICTURE_FILE_ID, mPictureFile);
			startActivity(intent);
		}
	}
	
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			draw();
		}
	}
}
