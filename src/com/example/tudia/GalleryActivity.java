package com.example.tudia;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

public class GalleryActivity extends Activity {
	public static final String GALLERY_RESULT_NAME = "image_file";
	private static final String IMAGES_ASSETS_FOLDER = "small_images";
	private Gallery mGallery;
	private Bitmap mImages[];

	/**
	 * Listener for clicking an image.
	 */
	private final OnItemClickListener mImageClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
			finishWithResult(mImages[position]);
		}
	};

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gallery);
		Window window = getWindow();
		if (window != null) {
			window.setLayout(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		}
		try {
			getImagesFromAssets();
		} catch (IOException e) {
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
		mGallery = (Gallery) findViewById(R.id.gallery);
		mGallery.setAdapter(new GalleryAdapter());
		mGallery.setOnItemClickListener(mImageClickListener);
	}

	/**
	 * Gets all file names from assets folder with small images.
	 * 
	 * @throws IOException
	 *             When cannot open an asset.
	 */
	private void getImagesFromAssets() throws IOException {
		AssetManager am = getAssets();
		String fileNames[] = am.list(IMAGES_ASSETS_FOLDER);
		mImages = new Bitmap[fileNames.length];
		for (int i = 0; i < fileNames.length; i++) {
			mImages[i] = BitmapFactory.decodeStream(am.open(IMAGES_ASSETS_FOLDER + File.separator + fileNames[i]));
		}
	}

	/**
	 * Finishes this activity with given bitmap as a result.
	 * 
	 * @param bitmap
	 *            Bitmap to be returned to the calling activity.
	 */
	private void finishWithResult(final Bitmap bitmap) {
		Intent intent = new Intent();
		intent.putExtra(GALLERY_RESULT_NAME, bitmap);
		if (getParent() == null) {
			setResult(Activity.RESULT_OK, intent);
		} else {
			getParent().setResult(Activity.RESULT_OK, intent);
		}
		finish();
	}

	/**
	 * Adapter for the small images Gallery.
	 */
	private class GalleryAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return mImages.length;
		}

		@Override
		public Object getItem(final int position) {
			return position;
		}

		@Override
		public long getItemId(final int position) {
			return position;
		}

		@Override
		public View getView(final int position, final View convertView, final ViewGroup parent) {
			ImageView imageView = new ImageView(GalleryActivity.this);
			imageView.setImageBitmap(mImages[position]);
			return imageView;
		}
	}
}
