package com.example.tudia;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.Window;

public class GetPlanActivity extends FragmentActivity {
	SectionPagerAdapter planSectionPagerAdapter;
	ViewPager planViewPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_get_plan);
		planSectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
		planViewPager = (ViewPager) findViewById(R.id.pager);
		planViewPager.setAdapter(planSectionPagerAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.the_girls, menu);
		return true;
	}

}