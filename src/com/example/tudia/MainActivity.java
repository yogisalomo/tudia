package com.example.tudia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import com.example.tudia.vai.activities.ActivitySelection;

public class MainActivity extends Activity {
	
	private OnClickListener getPlanListener = new OnClickListener(){
		public void onClick(View v){
			try{
				Intent k = new Intent(MainActivity.this,GetPlanActivity.class);
				startActivity(k);
			}catch(Exception e){
				Log.d("intent", "Cannot call getPlanActivity");
			}
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		Spinner destination = (Spinner) findViewById(R.id.destination);
		Button getPlan = (Button) findViewById(R.id.getPlan);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.city_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		destination.setAdapter(adapter);
		getPlan.setOnClickListener(getPlanListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		Intent intent;
		switch (item.getItemId()) {
		case R.id.act_select:
			intent = new Intent(this, ActivitySelection.class);
			startActivity(intent);
			return true;
		case R.id.cam:
			intent = new Intent(this, Cam.class);
			startActivity(intent);
			return true;
		default:
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

}
