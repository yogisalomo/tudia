package com.example.tudia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;


public class OneDayFragment extends Fragment {
	private OnClickListener getDetailListener = new OnClickListener(){
		public void onClick(View v){
			try{
				Intent k = new Intent(getActivity(),PlaceDetailActivity.class);
				startActivity(k);
			}catch(Exception e){
				Log.d("intent", "Cannot call PlaceDetailActivity");
			}
		}
	};
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				View rootView = inflater.inflate(R.layout.one_day, container, false);
				TextView BFName = (TextView) rootView.findViewById(R.id.BFName);
				TextView MName = (TextView) rootView.findViewById(R.id.MName);
				TextView LName = (TextView) rootView.findViewById(R.id.LName);
				TextView DName = (TextView) rootView.findViewById(R.id.DName);
				TextView SName = (TextView) rootView.findViewById(R.id.SName);
				TextView NName = (TextView) rootView.findViewById(R.id.NName);
				TextView STName = (TextView) rootView.findViewById(R.id.STName);
				BFName.setClickable(true);
				MName.setClickable(true);
				LName.setClickable(true);
				DName.setClickable(true);
				SName.setClickable(true);
				NName.setClickable(true);
				STName.setClickable(true);
				BFName.setOnClickListener(getDetailListener);
				MName.setOnClickListener(getDetailListener);
				LName.setOnClickListener(getDetailListener);
				DName.setOnClickListener(getDetailListener);
				SName.setOnClickListener(getDetailListener);
				NName.setOnClickListener(getDetailListener);
				STName.setOnClickListener(getDetailListener);
				return rootView;
			}

			 @Override
			 public void onResume(){
				 super.onResume();
			 }
			 
			 @Override
			 public void onPause(){
				 super.onPause();
			 }

}
