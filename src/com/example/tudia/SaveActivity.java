package com.example.tudia;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SaveActivity extends ListActivity {
	private static final Bitmap.CompressFormat COMPRESS_FORMAT = Bitmap.CompressFormat.PNG;
	private static final int COMPRESS_QUALITY = 100;
	
	File file = new File(Environment.getExternalStorageDirectory(), Cam.TMP_FILE_NAME);

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.save);
		initList();
		setActions();
	}

	/**
	 * Initializes ListView view.
	 */
	private void initList() {
		String[] saveOptions = getResources().getStringArray(R.array.save_options);
		ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, saveOptions);
		setListAdapter(adapter);
	}

	/**
	 * Defines actions connected with all ListView elements.
	 */
	private void setActions() {
		ListView list = getListView();
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
				switch (position) {
				case 0:
					// save to file
					saveToFile();
					break;
				case 1:
					// share picture
					sharePicture();
					break;
				default:
					break;
				}
			}
		});
	}

	/**
	 * Starts an activity for sharing an image.
	 */
	private void sharePicture() {

		OutputStream output = null;

		Bitmap bitmap = DrawActivity.getBitmap();
		if (bitmap != null) {
			try {
				output = new BufferedOutputStream(new FileOutputStream(file));
				bitmap.compress(COMPRESS_FORMAT, COMPRESS_QUALITY, output);
				Intent sendIntent = new Intent(Intent.ACTION_SEND);
				sendIntent.setType("image/png");
				sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
				startActivity(Intent.createChooser(sendIntent, getString(R.string.share)));
			} catch (FileNotFoundException e) {
				Toast.makeText(this, R.string.share_failed, Toast.LENGTH_SHORT).show();
			} finally {
				try {
					if (output != null) {
						output.close();
					}
				} catch (IOException e) {
					Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	/**
	 * Opens a dialog to save an image to a file.
	 */
	
	private void upload() {
		Intent intent = new Intent(this, com.example.tudia.vai.activities.ImageUploadTest.class);
		startActivity(intent);
	}
	
	private void saveToFile() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final EditText input = new EditText(this);
		builder.setTitle(getResources().getStringArray(R.array.save_options)[0])
				.setMessage(R.string.enter_picture_title).setView(input)
				.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						Bitmap bitmap = DrawActivity.getBitmap();
						if (bitmap != null) {
							if (MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, input.getText()
									.toString(), null) != null) {
								Toast.makeText(SaveActivity.this, R.string.save_ok, Toast.LENGTH_SHORT).show();
								SaveActivity.this.finish();
								ExifInterface exif;
								try {
									exif = new ExifInterface(file.getAbsolutePath());
									exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, "1");
									exif.setAttribute(ExifInterface.TAG_GPS_ALTITUDE, "1");
						            exif.saveAttributes();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									Log.d("SaveActivity",""+file.getAbsolutePath());
								}
								upload();
								return;
							}
						}
						Toast.makeText(SaveActivity.this, R.string.save_error, Toast.LENGTH_SHORT).show();
						SaveActivity.this.finish();
					}
				}).show();
	}
}
