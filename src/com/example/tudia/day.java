package com.example.tudia;

public class day {
	restaurant breakfast;
	venue morning;
	restaurant lunch;
	venue day;
	restaurant supper;
	venue night;
	stay accomodation;
	public day(restaurant bf, venue mrn, restaurant lch, venue d, restaurant sp, venue n, stay acc){
		breakfast = bf;
		morning = mrn;
		lunch = lch;
		day = d;
		supper = sp;
		night = n;
		accomodation = acc;
	}
	public restaurant getBreakfast(){
		return breakfast;
	}
	public venue getMorning(){
		return morning;
	}
	public restaurant getLunch(){
		return lunch;
	}
	public venue getDay(){
		return day;
	}
	public restaurant getSupper(){
		return supper;
	}
	public venue getNight(){
		return night;
	}
	public stay getAccomodation(){
		return accomodation;
	}
	public void setBreakfast(restaurant _breakfast){
		breakfast = _breakfast;
	}
	public void setMorning(venue _morning){
		morning = _morning;
	}
	public void setLunch(restaurant _lunch){
		lunch = _lunch;
	}
	public void setDay(venue _day){
		day = _day;
	}
	public void setSupper(restaurant _supper){
		supper = _supper;
	}
	public void setNight(venue _night){
		night = _night;
	}
	public void setAccomodation(stay _accomodation){
		accomodation = _accomodation;
	}
}
