package com.example.tudia;

import java.util.ArrayList;

public class people {
	private int id;
	private String name;
	private String title; // Untuk unsur gamification, berubah berdasarkan jumlah review, jumlah rencana, jumlah tempat dikunjungi, dll
	private ArrayList<place> placeVisited;
	private ArrayList<plan> planMade;
	private ArrayList<review> reviewMade;
	
	public people(int _id, String _name){
		id = _id;
		name = _name;
		title = "Beginner";
		placeVisited = new ArrayList<place>();
		planMade = new ArrayList<plan>();
		reviewMade = new ArrayList<review>();
	}
	public String getName(){
		return name;
	}
	public int getId(){
		return id;
	}
	public String getTitle(){
		return title;
	}
	public ArrayList<place> getPlaceVisited(){
		return placeVisited;
	}
	public ArrayList<plan> getPlanMade(){
		return planMade;
	}
	public ArrayList<review> getReviewMade(){
		return reviewMade;
	}
	public void setTitle(String nt){
		title = nt;
	}
	public void addPlaceVisited(place p){
		placeVisited.add(p);
	}
	public void addPlanMade(plan pl){
		planMade.add(pl);
	}
	public void addReviewMade(review r){
		reviewMade.add(r);
	}
}
