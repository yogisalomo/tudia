package com.example.tudia;

public class place {
	private int id;
	private String name;
	private String description;
	private String address;
	private int votes; //The total of votes the place got
	private int voters; // The number of Voters already voted the place
	private int minBudget;
	private int maxBudget;
	private long latitude;
	private long longitude;
	
	public place(int _id, String _name, String _description, String _address, int _minBudget, int _maxBudget, long _latitude, long _longitude){
		id =  _id;
		name = _name;
		description = _description;
		address = _address;
		votes = 0; 
		voters = 0; 
		minBudget = _minBudget;
		maxBudget = _maxBudget;
		latitude = _latitude;
		longitude = _longitude;
	}
	public int getId(){
		return id;
	}
	public String getName(){
		return name;
	}
	public String getDescription(){
		return description;
	}
	public String getAddress(){
		return address;
	}
	public float getRating(){
		return votes/ voters;
	}
	public int getMinBudget(){
		return minBudget;
	}
	public int getMaxBudget(){
		return maxBudget;
	}
	public long getLatitude(){
		return latitude;
	}
	public long getLongitude(){
		return longitude;
	}
	public void addVote(int newVote){
		votes+= newVote;
		voters+=1;
	}
}
