package com.example.tudia;

import java.util.ArrayList;
import java.util.Date;

public class plan {
	private int id;
	private int budget;
	private int owner_id;
	private Date beginDate;
	private Date endDate;
	private ArrayList<day> days;
	
	public plan(int _id, int _budget, int _owner, Date bDate, Date eDate, ArrayList<day> _days){
		id = _id;
		budget = _budget;
		owner_id = _owner;
		beginDate = bDate;
		endDate = eDate;
		days = _days;
	}
	public int getId(){
		return id;
	}
	public int getBudget(){
		return budget;
	}
	public int getOwner(){
		return owner_id;
	}
	public Date getBeginDate(){
		return beginDate;
	}
	public Date getEndDate(){
		return endDate;
	}
	public ArrayList<day> getDays(){
		return days;
	}
}
