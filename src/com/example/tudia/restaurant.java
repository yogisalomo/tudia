package com.example.tudia;

public class restaurant extends place {
	private String topMenu;
	public restaurant(int _id, String _name, String _description,
			String _address, int _minBudget, int _maxBudget, long _latitude,
			long _longitude, String _topMenu) {
		super(_id, _name, _description, _address, _minBudget, _maxBudget,
				_latitude, _longitude);
		// TODO Auto-generated constructor stub
		topMenu = _topMenu;
	}
	public String getTopMenu(){
		return topMenu;
	}
	public void setTopMenu(String newTop){
		topMenu = newTop;
	}
}
