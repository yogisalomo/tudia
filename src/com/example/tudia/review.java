package com.example.tudia;

public class review {
	int id;
	int place_id;
	String content;
	
	public review(int _id, int _place, String _content){
		id=_id;
		place_id = _place;
		content = _content;
	}
	public int getId(){
		return id;
	}
	public int getPlace(){
		return place_id;
	}
	public String getContent(){
		return content;
	}
	public void setContent(String _content){
		content = _content;
	}
}