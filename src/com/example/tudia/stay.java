package com.example.tudia;

public class stay extends place {
	private boolean incBreakfast; //To check whether the stay place includes breakfast package
	public stay(int _id, String _name, String _description, String _address,
			int _minBudget, int _maxBudget, long _latitude, long _longitude, boolean _breakfast) {
		super(_id, _name, _description, _address, _minBudget, _maxBudget,
				_latitude, _longitude);
		// TODO Auto-generated constructor stub
		incBreakfast = _breakfast;
	}
	public boolean getIncBreakfast(){
		return incBreakfast;
	}
}
