package com.example.tudia.vai.activities;


import com.example.tudia.R;
import com.parse.Parse;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ActivitySelection extends ListActivity {

	public static final String PARSE_MODE = "usingParse";
	
	private static final int CALENDAR_PICKER = 0;
	private static final int SQLITE_DATABASE = 1;
	private static final int PARSE_DATABASE = 2;
	private static final int PARSE_USER_ACCOUNT = 3;
	private static final int PAGE_INTERFACE = 4;
	private static final int IMAGE_UPLOAD = 5;
	
	public static String CURRENT_USER = "anonymous";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Settings.System.putString(getContentResolver(), Settings.System.HTTP_PROXY, "mohamad.rivai:stimatubes@cache.itb.ac.id:8080");
		setContentView(R.layout.activity_list);
		String[] items = new String[] {"Date and Time Picker", "SQLite Database", "Parse Database", "Parse Login", "Review List Page", "Upload Image"};
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.activity_list_row, R.id.text1, items);
		setListAdapter(adapter);
		Parse.initialize(this, "OgnRLUxKy4hAmFczNpvyLRM3EwYi9Vmacu87tTpn", "qTo7hpVe6ixS1BGIkUGcuN6kua7ZRUM1y6hM6bSW");
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id){
		super.onListItemClick(l, v, position, id);
		Intent intent;
		switch (position) {
		case CALENDAR_PICKER:
			intent = new Intent(this, CalendarPicker.class);
			startActivity(intent);
			break;
		case SQLITE_DATABASE:
			intent = new Intent(this, ReviewListTest.class);
			startActivity(intent);
			break;
		case PARSE_DATABASE:
			intent = new Intent(this, ReviewListTest.class);
			intent.putExtra(ActivitySelection.PARSE_MODE, "Active");
			startActivity(intent);
			break;
		case PARSE_USER_ACCOUNT:
			intent = new Intent(this, ParseAccountTest.class);
			startActivity(intent);
			break;
		case PAGE_INTERFACE:
			intent = new Intent(this, LocationListPage.class);
			//intent.putExtra(ActivitySelection.PARSE_MODE, "Active");
			startActivity(intent);
			break;
		case IMAGE_UPLOAD:
			intent = new Intent(this, ImageUploadTest.class);
			startActivity(intent);
			break;
		default:
			break;
		}
	}

}
