package com.example.tudia.vai.activities;

import com.example.tudia.R;
import com.example.tudia.vai.utilities.DatePickHandler;
import com.example.tudia.vai.utilities.TimePickHandler;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class CalendarPicker extends Activity {

	
    
    
	DatePickHandler dateHandler;
	TextView dateDisplay;
	Button datePicker;
	
	
	TimePickHandler timeHandler;
	TextView timeDisplay;
	Button timePicker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calendar_picker);
		
		dateDisplay = (TextView) findViewById(R.id.dateDisplay);
		Button datePicker = (Button) findViewById(R.id.datePicker);
		dateHandler = new DatePickHandler(this, dateDisplay);
		datePicker.setOnClickListener(dateHandler);
		
		timeDisplay = (TextView) findViewById(R.id.timeDisplay);
		Button timePicker = (Button) findViewById(R.id.timePicker);
		timeHandler = new TimePickHandler(this, timeDisplay);
		timePicker.setOnClickListener(timeHandler);
	}
	
	@Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DatePickHandler.DATE_DIALOG_ID:
                return dateHandler.createDatePickerDialog();
            case TimePickHandler.TIME_DIALOG_ID:
            	return timeHandler.createTimePickerDialog();
        }
        return null;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case DatePickHandler.DATE_DIALOG_ID:
            	dateHandler.prepareDatePickerDialog(dialog);
                break;
            case TimePickHandler.TIME_DIALOG_ID:
            	timeHandler.prepareTimePickerDialog(dialog);
            	break;
        }
    }

}
