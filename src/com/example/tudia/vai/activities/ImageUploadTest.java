package com.example.tudia.vai.activities;

import java.io.ByteArrayOutputStream;

import com.example.tudia.R;
import com.parse.ParseFile;
import com.parse.ParseObject;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageUploadTest extends Activity {
	
	private static int SELECT_IMAGE = 4252;
	
	Button uploadButton;
	EditText name;
	Bitmap mSelectedImage;
	String file_name;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_upload);
		
		name = (EditText) findViewById(R.id.location_name);
		Button buttonLoadImage = (Button) findViewById(R.id.upload_buttonLoadPicture);
        buttonLoadImage.setOnClickListener(new View.OnClickListener() {
             
            @Override
            public void onClick(View arg0) {
                 
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                 
                startActivityForResult(i, SELECT_IMAGE);
            }
        });
        uploadButton = (Button) findViewById(R.id.upload_buttonUploadPicture);
        uploadButton.setOnClickListener(new View.OnClickListener() {
             
            @Override
            public void onClick(View arg0) {
            	Bitmap bitmap = mSelectedImage;//= BitmapFactory.decodeResource(getResources(),R.drawable.androidbegin);
                
            	if (bitmap == null) return;
            	
            	// Convert it to byte
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // Compress image to lower quality scale 1 - 100
                //bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] image = stream.toByteArray();
 
                // Create the ParseFile
                ParseFile file = new ParseFile(file_name, image);
                // Upload the image into Parse Cloud
                file.saveInBackground();
 
                // Create a New Class called "ImageUpload" in Parse
                ParseObject imgupload = new ParseObject("photos");
 
                // Create a column named "ImageName" and set the string
                imgupload.put("name", name.getEditableText().toString());
 
                // Create a column named "ImageFile" and insert the image
                imgupload.put("ImageFile", file);
 
                // Create the class and the columns
                imgupload.saveInBackground();
                Toast.makeText(ImageUploadTest.this, "Image Uploaded", Toast.LENGTH_SHORT).show();
//                try{
//	                imgupload.save();
//	                // Show a simple toast message
//	                Toast.makeText(ImageUploadTest.this, "Image Uploaded",
//	                        Toast.LENGTH_SHORT).show();
//                } catch (ParseException e){
//                	Toast.makeText(ImageUploadTest.this, "Upload failed " + e.getMessage(),
//	                        Toast.LENGTH_SHORT).show();
//                	e.printStackTrace();
//                }
            }
        });
        uploadButton.setEnabled(false);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		 super.onActivityResult(requestCode, resultCode, data);
         
        if (requestCode == SELECT_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
 
            if (selectedImage.getScheme().toString().compareTo("content")==0)
            {      
                Cursor cursor =getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor.moveToFirst())
                {
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
                    String picturePath = cursor.getString(column_index);
                    
                    Uri filePathUri = Uri.parse(picturePath);
                    
                    cursor.close();
                    ImageView imageView = (ImageView) findViewById(R.id.upload_imgView);
                    mSelectedImage = BitmapFactory.decodeFile(picturePath);
                    imageView.setImageBitmap(mSelectedImage);
                    
                    
                    file_name = filePathUri.getLastPathSegment().toString();
                    //String file_path=filePathUri.getPath();
                    Toast.makeText(this,"File Name :"+file_name, Toast.LENGTH_LONG).show();
                }
            }
            
 
            
            
//            try{
//            	InputStream imageStream = getContentResolver().openInputStream(selectedImage);
//                mSelectedImage = BitmapFactory.decodeStream(imageStream);
//                ImageView imageView = (ImageView) findViewById(R.id.upload_imgView);
//                imageView.setImageBitmap(mSelectedImage);
//                //Toast.makeText(ImageUploadTest.this, "File Found " + selectedImage.getPath(), Toast.LENGTH_SHORT).show();
//            	
//            } catch (FileNotFoundException e){
//            	Toast.makeText(ImageUploadTest.this, "File Not Found", Toast.LENGTH_SHORT).show();
//            	e.printStackTrace();
//            	return;
//            }
            
            
            uploadButton.setEnabled(true);
        }
	}

}
