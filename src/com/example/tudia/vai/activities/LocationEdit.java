package com.example.tudia.vai.activities;

import com.example.tudia.R;
import com.example.tudia.vai.storage.LocationBean;
import com.example.tudia.vai.storage.LocationDataSource;
import com.example.tudia.vai.storage.oncloud.ParseLocationDataSource;
import com.example.tudia.vai.storage.ondevice.SQLiteLocationDataSource;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class LocationEdit extends Activity {

	//TODO:
		//validate input
		
		TextView mRatingText;
		Button saveButton;
		int numStars;
		
		EditText titleText;
		Spinner citySpinner;
		RatingBar rating;
		EditText bodyText;
		
		String id;
		String parseMode;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_location_edit);

	        id = getIntent().getStringExtra(LocationBean.LOCATION_TABLE_ID);

			parseMode = getIntent().getStringExtra(ActivitySelection.PARSE_MODE);

			
			titleText = (EditText) findViewById(R.id.location_title);
			citySpinner = (Spinner) findViewById(R.id.location_edit_spinner);
			bodyText = (EditText) findViewById(R.id.location_body);
			
			
			saveButton = (Button) findViewById(R.id.location_confirm);
			saveButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					LocationBean bean;
					if (parseMode != null){
						Log.d("EDIT", "Location Info Saved");
						bean = new LocationBean(titleText.getText().toString(),citySpinner.getSelectedItem().toString(), rating.getRating(), bodyText.getText().toString());
					}
					else {
						bean = new LocationBean();
						bean.setTitle(titleText.getText().toString());
						bean.setCity(citySpinner.getSelectedItem().toString());
						bean.setRating(rating.getRating());
						bean.setBody(bodyText.getText().toString());
					}
					
					
					LocationDataSource datasource;
					if (parseMode != null) datasource = new ParseLocationDataSource(LocationEdit.this); 
					else datasource = new SQLiteLocationDataSource(LocationEdit.this);
					
					datasource.open();
					if (id != null){
						LocationBean _bean;
						if (parseMode != null){
							_bean = datasource.get(id);
							_bean.setBody(bean.getBody());
							_bean.setCity(bean.getCity());
							_bean.setRating(bean.getRating());
							_bean.setTitle(bean.getTitle());
						} else {
							_bean = bean;
						}
						//Toast.makeText(LocationEdit.this, "Update succeed. ID: " + _bean.getId(), Toast.LENGTH_SHORT).show();
						Log.d("EDIT", "ID1 : " + _bean.getId());
						_bean.setId(id);
						datasource.update(_bean);
					}
					else datasource.insert(bean);
					datasource.close();
					onBackPressed();
				}
			});
			
			mRatingText = (TextView) findViewById(R.id.location_rating_text);

	        // The different rating bars in the layout. Assign the listener to us.
	        rating = ((RatingBar)findViewById(R.id.location_ratingbar));
	        numStars = rating.getNumStars();
	        mRatingText.setText("Rating: " + rating.getRating() + "/" + numStars);
	        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
				
				@Override
				public void onRatingChanged(RatingBar ratingBar, float ratingValue,
						boolean fromUser) {
					// TODO Auto-generated method stub
					mRatingText.setText( 
			                "Rating: " + ratingValue + "/" + numStars);
				}
			});
	        
			if (id != null) populateFields();
			
	        
		}
		
		public void populateFields(){
			LocationDataSource datasource;
			if (parseMode != null) datasource = new ParseLocationDataSource(this); 
			else datasource = new SQLiteLocationDataSource(this);
			
			datasource.open();
			LocationBean bean = datasource.get(id);
			
			titleText.setText(bean.getTitle());
			bodyText.setText(bean.getBody());
			rating.setRating(bean.getRating());	
			mRatingText.setText("Rating: " + rating.getRating() + "/" + rating.getNumStars());
			
			datasource.close();
		}

}
