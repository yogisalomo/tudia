package com.example.tudia.vai.activities;

import java.util.ArrayList;
import java.util.List;

import com.example.tudia.R;
import com.example.tudia.vai.storage.LocationBean;
import com.example.tudia.vai.storage.LocationDataSource;
import com.example.tudia.vai.storage.oncloud.ParseLocationDataSource;
import com.example.tudia.vai.storage.ondevice.SQLiteLocationDataSource;
import com.parse.Parse;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class LocationListPage extends ListActivity {

	private static final int INSERT_LOCATION = 0;
	private static final int EDIT_LOCATION = 1;
	
	private LocationDataSource datasource;
	List<LocationBean> locations;
	
	String parseMode;
	
	//public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//View view = inflater.inflate(R.layout.activity_review_list, container, false);
		setContentView(R.layout.activity_review_list);
		Parse.initialize(this, "OgnRLUxKy4hAmFczNpvyLRM3EwYi9Vmacu87tTpn", "qTo7hpVe6ixS1BGIkUGcuN6kua7ZRUM1y6hM6bSW");
		//parseMode = getIntent().getStringExtra(ActivitySelection.PARSE_MODE);
		parseMode = "active";
		if (parseMode != null) datasource = new ParseLocationDataSource(this); 
		else datasource = new SQLiteLocationDataSource(this);
		datasource.open();
		
		fillData();
		
		//registerForContextMenu(getListView());
		
		
		//setHasOptionsMenu(true);
		//return view;
	}
	
	public void fillData(){
//		String[] items = new String[] {"satu", "dua", "tiga", "empat"};
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.activity_list_row, R.id.text1, items);

		
		locations = new ArrayList<LocationBean>(datasource.getAll());
//				List<ReviewBean>(datasource.getAllReviews());
		List<String> items = new ArrayList<String>();
		int counter = 0;
		for (LocationBean bean : locations) {
			String text = (++counter) + ". " + bean.getTitle() + " (" + bean.getCity() + ")";
			items.add(text);
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.activity_list_row, R.id.text1, items);
		
		//Toast.makeText(this, "Retrieved succeed with size : " + items.size(), Toast.LENGTH_SHORT).show();
		
		setListAdapter(adapter);
		
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
		super.onListItemClick(l, v, position, id);
		Intent i = new Intent(this, ReviewLocationPage.class);
		i.putExtra(LocationBean.LOCATION_TABLE_ID, locations.get((int) id).getId());
		i.putExtra(LocationBean.LOCATION_TABLE_TITLE, locations.get((int) id).getTitle());
		//Toast.makeText(this, "Populate Display Parse" + i.getExtras(), Toast.LENGTH_SHORT).show();
		if (parseMode != null) i.putExtra(ActivitySelection.PARSE_MODE, parseMode);
		startActivity(i);
	}
	
	/*
	@SuppressLint({ "InlinedApi", "NewApi" })
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Menu items default to never show in the action bar. On most devices this means
        // they will show in the standard options menu panel when the menu button is pressed.
        // On xlarge-screen devices a "More" button will appear in the far right of the
        // Action Bar that will display remaining items in a cascading menu.
        //menu.add("Normal item");
		//super.onCreateOptionsMenu(menu, inflater);
        MenuItem actionItem = menu.add("Add Button");
        

        // Items that show as actions should favor the "if room" setting, which will
        // prevent too many buttons from crowding the bar. Extra items will show in the
        // overflow area.
        //actionItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        actionItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        
        // Items that show as actions are strongly encouraged to use an icon.
        // These icons are shown without a text description, and therefore should
        // be sufficiently descriptive on their own.
        actionItem.setIcon(android.R.drawable.ic_menu_add);

        return true;
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		Intent i = new Intent(this, LocationEdit.class);
		switch (item.getItemId()) {
		case INSERT_LOCATION:
			if (parseMode != null) i.putExtra(ActivitySelection.PARSE_MODE, parseMode);
			startActivityForResult(i, INSERT_LOCATION);
			break;
		}
		return true;
    }
	*/
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent){
		super.onActivityResult(requestCode, requestCode, intent);
		fillData();
	}
	
	/*
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo){
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu__review_list_longpress, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item){
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()){
		case R.id.delete_review:
			datasource.delete(locations.get((int) info.id).getId());
			fillData();
			break;
		case R.id.edit_review:
			Intent i = new Intent(this, LocationEdit.class);
			i.putExtra(LocationBean.LOCATION_TABLE_ID, locations.get((int) info.id).getId());
			if (parseMode != null) i.putExtra(ActivitySelection.PARSE_MODE, parseMode);
			startActivityForResult(i, EDIT_LOCATION);
			break;
		}
		return super.onContextItemSelected(item);
	}
	*/

}


