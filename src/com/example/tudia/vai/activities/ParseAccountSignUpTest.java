package com.example.tudia.vai.activities;

import com.example.tudia.R;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ParseAccountSignUpTest extends Activity {

	EditText nameText;
	EditText passText;
	EditText confirmPassText;
	EditText mailText;
	Button createButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parse_account_signup);
		Parse.initialize(this, "OgnRLUxKy4hAmFczNpvyLRM3EwYi9Vmacu87tTpn", "qTo7hpVe6ixS1BGIkUGcuN6kua7ZRUM1y6hM6bSW");
		
		nameText = (EditText) findViewById(R.id.editTextUserName);
		passText = (EditText) findViewById(R.id.editTextPassword);
		confirmPassText = (EditText) findViewById(R.id.editTextConfirmPassword);
		mailText = (EditText) findViewById(R.id.editTextEmail);
		
		createButton = (Button) findViewById(R.id.buttonCreateAccount);
		createButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				signUp();
				onBackPressed();
			}
		});
	}
	
	public void signUp(){
		String name = nameText.getText().toString();
		String pass = passText.getText().toString();
		String confirmPass = confirmPassText.getText().toString();
		String mail = mailText.getText().toString();
		
		if (name.equals("") || pass.equals("") || confirmPass.equals("")){
			Toast.makeText(this, "All fields must be filled.", Toast.LENGTH_SHORT).show();
			return;
		}
		
		if (!pass.contentEquals(confirmPass)){
			Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show();
			return;
		}
		
		ParseUser user = new ParseUser();
		user.setUsername(name);
		user.setPassword(pass);
		if (!mail.equals("")) user.setEmail(mail);
		 
		// other fields can be set just like with ParseObject
		//user.put("phone", "650-253-0000");
		 
//		user.signUpInBackground(new SignUpCallback() {
//			
//			@Override
//			public void done(ParseException e) {
//				// TODO Auto-generated method stub
//				if (e == null) {
//			      // Hooray! Let them use the app now.
//					Toast.makeText(ParseAccountSignUpTest.this, "Sign Up Succeed.", Toast.LENGTH_SHORT).show();
//					return;
//			    } else {
//			      // Sign up didn't succeed. Look at the ParseException
//			      // to figure out what went wrong
//			    	Toast.makeText(ParseAccountSignUpTest.this, "Sign Up Failed.", Toast.LENGTH_SHORT).show();
//			    	e.printStackTrace();
//			    	return;
//			    }
//			}
//		});
		try{
			user.signUp();
			Toast.makeText(ParseAccountSignUpTest.this, "Sign Up Succeed.", Toast.LENGTH_SHORT).show();
			return;
		} catch(ParseException e){
			Toast.makeText(ParseAccountSignUpTest.this, "Sign Up Failed.", Toast.LENGTH_SHORT).show();
	    	e.printStackTrace();
	    	
		}
	}

}
