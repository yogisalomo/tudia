package com.example.tudia.vai.activities;

import com.example.tudia.R;
import com.parse.Parse;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ParseAccountTest extends Activity {

	
	TextView helloText;
	Button logButton;
	Button signButton;
	
	Button loginButton;
	
	ParseUser user;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parse_account);
		Parse.initialize(this, "OgnRLUxKy4hAmFczNpvyLRM3EwYi9Vmacu87tTpn", "qTo7hpVe6ixS1BGIkUGcuN6kua7ZRUM1y6hM6bSW");
		helloText = (TextView) findViewById(R.id.hello);
		logButton = (Button) findViewById(R.id.button1);
		signButton = (Button) findViewById(R.id.button2);
		
//		ParseUser currentUser = ParseUser.getCurrentUser();
//		if (currentUser != null) {
//		  // do stuff with the user
//			signButton.setEnabled(false);
//			logButton.setText("Log out");
//		} else {
//		  // show the signup or login screen
//			ParseAnonymousUtils.logIn(new LogInCallback() {
//				
//				@Override
//				public void done(ParseUser u, ParseException e) {
//					// TODO Auto-generated method stub
//					if (e != null) {
//						Toast.makeText(ParseAccountTest.this, "Anonymous login failed", Toast.LENGTH_SHORT);
//						Log.d("MyApp", "Anonymous login failed.");
//				    } else {
//				    	Toast.makeText(ParseAccountTest.this, "Anonymous user logged in", Toast.LENGTH_SHORT);
//						Log.d("MyApp", "Anonymous user logged in.");
//						user = u;
//				    }
//				}
//			});
//		}
//		
//		
//		
//		

		
		
		
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		ParseUser.enableAutomaticUser();
		ParseUser.getCurrentUser().increment("RunCount");
		//ParseUser.getCurrentUser().saveInBackground();
		
		if (ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())) {
			setEnableSignUp();
			setEnableLogIn();
		} else {
			setDisableSignUp();
			setEnableLogOut();
		}
	}
	
	private void setEnableSignUp(){
		ActivitySelection.CURRENT_USER = "anonymous";
		helloText.setText("Hello, " + ActivitySelection.CURRENT_USER);
		signButton.setEnabled(true);
		signButton.setVisibility(Button.VISIBLE);
		
		signButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ParseAccountTest.this, ParseAccountSignUpTest.class);
				startActivity(i);
			}
		});
	}
	
	private void setDisableSignUp(){
		ActivitySelection.CURRENT_USER = ParseUser.getCurrentUser().getUsername();
		helloText.setText("Hello, " + ActivitySelection.CURRENT_USER);
		signButton.setEnabled(false);
		signButton.setVisibility(Button.INVISIBLE);
		
	}
	
	private void setEnableLogIn(){
		logButton.setText("Log In");
		
		logButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showLogInDialog(v);
			}
		});
	}
	
	private void setEnableLogOut(){
		logButton.setText("Log out");
		
		logButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				logOut();
			}
		});
	}
	
	
	
	public void showLogInDialog(View v){
		final Dialog dialog = new Dialog(ParseAccountTest.this);

        dialog.setContentView(R.layout.activity_parse_account_login);
        dialog.setTitle("Login");
        
        final EditText nameText = (EditText) dialog.findViewById(R.id.editTextUserNameToLogin);
        final EditText passText = (EditText) dialog.findViewById(R.id.editTextPasswordToLogin);
        nameText.getText();
        loginButton = (Button) dialog.findViewById(R.id.buttonSignIn);
        loginButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				ParseUser.logIn(nameText.getEditableText().toString(), passText.getEditableText().toString(), new LogInCallback() {
//				  public void done(ParseUser user, ParseException e) {
//				    if (user != null) {
//				      // Hooray! The user is logged in.
//				    	Toast.makeText(ParseAccountTest.this, "Login Succeed", Toast.LENGTH_SHORT);
//				    	setDisableSignUp();
//				    	//helloText.setText("Hello, " + user.getUsername());
//				    } else {
//				    	Toast.makeText(ParseAccountTest.this, "Login failed " + e.getMessage(), Toast.LENGTH_SHORT);
//				      // Signup failed. Look at the ParseException to see what happened.
//				    }
//				  }
//				});
				try{
					ParseUser.logIn(nameText.getEditableText().toString(), passText.getEditableText().toString());
					Toast.makeText(ParseAccountTest.this, "Login Succeed", Toast.LENGTH_SHORT).show();
			    	setEnableLogOut();
					setDisableSignUp();
			    } catch (ParseException e){
					Toast.makeText(ParseAccountTest.this, "Login failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				      //
				}
				dialog.dismiss();
				
			}
		});
        dialog.show();
	}
	
//	public void signUp(){
//		ParseUser user = new ParseUser();
//		user.setUsername("my name");
//		user.setPassword("my pass");
//		user.setEmail("email@example.com");
//		 
//		// other fields can be set just like with ParseObject
//		user.put("phone", "650-253-0000");
//		 
//		user.signUpInBackground(new SignUpCallback() {
//			
//			@Override
//			public void done(ParseException e) {
//				// TODO Auto-generated method stub
//				if (e == null) {
//			      // Hooray! Let them use the app now.
//			    } else {
//			      // Sign up didn't succeed. Look at the ParseException
//			      // to figure out what went wrong
//			    }
//			}
//		});
//	}
	
//	public void logIn(){
//		ParseUser.logInInBackground("Jerry", "showmethemoney", new LogInCallback() {
//		  public void done(ParseUser user, ParseException e) {
//		    if (user != null) {
//		      // Hooray! The user is logged in.
//		    	helloText.setText("Hello, " + user.getUsername());
//		    } else {
//		      // Signup failed. Look at the ParseException to see what happened.
//		    }
//		  }
//		});
//	}
	
	public void logOut(){
		ParseUser.logOut();
		setEnableSignUp();
		setEnableLogIn();
	}

}
