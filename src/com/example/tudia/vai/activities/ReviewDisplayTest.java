package com.example.tudia.vai.activities;

import com.example.tudia.R;
import com.example.tudia.vai.storage.ReviewBean;
import com.example.tudia.vai.storage.ReviewDataSource;
import com.example.tudia.vai.storage.oncloud.ParseReviewDataSource;
import com.example.tudia.vai.storage.ondevice.SQLiteReviewDataSource;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class ReviewDisplayTest extends Activity {

	TextView title;
	TextView city;
	TextView reviewer;
	TextView body;
	TextView ratingText;
	RatingBar ratingBar;
	
	String id;
	String parseMode;
	
	ReviewBean bean;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_review_display);
	
		parseMode = getIntent().getStringExtra(ActivitySelection.PARSE_MODE);
		
		
		title = (TextView) findViewById(R.id.title);
		city = (TextView) findViewById(R.id.city);
		reviewer = (TextView) findViewById(R.id.reviewer);
		body = (TextView) findViewById(R.id.body);
		ratingText = (TextView) findViewById(R.id.rating_text);
		ratingBar = (RatingBar) findViewById(R.id.ratingbar);
		
		id = getIntent().getStringExtra(ReviewBean.REVIEW_TABLE_ID);
		if (id != null) populateFields();
		//else Toast.makeText(this, "No Populate Display Parse", Toast.LENGTH_SHORT).show();
	}
	
	public void populateFields(){
		ReviewDataSource datasource;
		if (parseMode != null){
			//Toast.makeText(this, "Populate Display Parse, ID" + id, Toast.LENGTH_SHORT).show();
			datasource = new ParseReviewDataSource(this); 
		}
		else{
			//Toast.makeText(this, "Populate Display SQL", Toast.LENGTH_SHORT).show();
			datasource = new SQLiteReviewDataSource(this);
		}
		
		datasource.open();
		bean = datasource.getReview(id);
		if (bean == null) return;
		title.setText(bean.getTitle());
		city.setText(bean.getCity());
		reviewer.setText(bean.getReviewer());
		body.setText(bean.getBody());
		ratingBar.setRating(bean.getRating());	
		ratingText.setText("Rating: " + ratingBar.getRating() + "/" + ratingBar.getNumStars());
		
		datasource.close();
	}

}
