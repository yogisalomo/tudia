package com.example.tudia.vai.activities;

import com.example.tudia.R;
import com.example.tudia.vai.storage.ReviewBean;
import com.example.tudia.vai.storage.ReviewDataSource;
import com.example.tudia.vai.storage.oncloud.ParseReviewDataSource;
import com.example.tudia.vai.storage.ondevice.SQLiteReviewDataSource;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ReviewEditTest extends Activity {

	//TODO:
	//validate input
	
	TextView mRatingText;
	Button saveButton;
	int numStars;
	
	EditText titleText;
	Spinner citySpinner;
	RatingBar rating;
	EditText reviewerText;
	EditText bodyText;
	
	String id;
	String parseMode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_review_edit);

        id = getIntent().getStringExtra(ReviewBean.REVIEW_TABLE_ID);

		parseMode = getIntent().getStringExtra(ActivitySelection.PARSE_MODE);

		
		titleText = (EditText) findViewById(R.id.title);
		citySpinner = (Spinner) findViewById(R.id.review_edit_spinner);
		reviewerText = (EditText) findViewById(R.id.reviewer);
		reviewerText.setText(ActivitySelection.CURRENT_USER);
		bodyText = (EditText) findViewById(R.id.body);
		
		
		saveButton = (Button) findViewById(R.id.confirm);
		saveButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ReviewBean bean;
				if (parseMode != null){
					Log.d("EDIT", "Review Saved");
					bean = new ReviewBean(titleText.getText().toString(),citySpinner.getSelectedItem().toString(), reviewerText.getText().toString(), rating.getRating(), bodyText.getText().toString());
				}
				else {
					bean = new ReviewBean();
					bean.setTitle(titleText.getText().toString());
					bean.setCity(citySpinner.getSelectedItem().toString());
					bean.setRating(rating.getRating());
					bean.setReviewer(reviewerText.getText().toString());
					bean.setBody(bodyText.getText().toString());
				}
				
				
				ReviewDataSource datasource;
				if (parseMode != null) datasource = new ParseReviewDataSource(ReviewEditTest.this); 
				else datasource = new SQLiteReviewDataSource(ReviewEditTest.this);
				
				datasource.open();
				if (id != null){
					ReviewBean _bean;
					if (parseMode != null){
						_bean = datasource.getReview(id);
						_bean.setBody(bean.getBody());
						_bean.setCity(bean.getCity());
						_bean.setRating(bean.getRating());
						_bean.setReviewer(bean.getReviewer());
						_bean.setTitle(bean.getTitle());
					} else {
						_bean = bean;
					}
					//Toast.makeText(ReviewEditTest.this, "Update succeed. ID: " + _bean.getId(), Toast.LENGTH_SHORT).show();
					Log.d("EDIT", "ID1 : " + _bean.getId());
					_bean.setId(id);
					datasource.updateReview(_bean);
				}
				else datasource.insertReview(bean);
				datasource.close();
				onBackPressed();
			}
		});
		
		mRatingText = (TextView) findViewById(R.id.rating_text);

        // The different rating bars in the layout. Assign the listener to us.
        rating = ((RatingBar)findViewById(R.id.ratingbar));
        numStars = rating.getNumStars();
        mRatingText.setText("Rating: " + rating.getRating() + "/" + numStars);
        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
			
			@Override
			public void onRatingChanged(RatingBar ratingBar, float ratingValue,
					boolean fromUser) {
				// TODO Auto-generated method stub
				mRatingText.setText( 
		                "Rating: " + ratingValue + "/" + numStars);
			}
		});
        
		if (id != null) populateFields();
		
        
	}
	
	public void populateFields(){
		ReviewDataSource datasource;
		if (parseMode != null) datasource = new ParseReviewDataSource(this); 
		else datasource = new SQLiteReviewDataSource(this);
		
		datasource.open();
		ReviewBean bean = datasource.getReview(id);
		
		titleText.setText(bean.getTitle());
		reviewerText.setText(bean.getReviewer());
		bodyText.setText(bean.getBody());
		rating.setRating(bean.getRating());	
		mRatingText.setText("Rating: " + rating.getRating() + "/" + rating.getNumStars());
		
		datasource.close();
	}

}
