package com.example.tudia.vai.activities;

import java.util.ArrayList;
import java.util.List;

import com.example.tudia.R;
import com.example.tudia.vai.storage.ReviewBean;
import com.example.tudia.vai.storage.ReviewDataSource;
import com.example.tudia.vai.storage.oncloud.ParseReviewDataSource;
import com.example.tudia.vai.storage.ondevice.SQLiteReviewDataSource;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ReviewListTest extends ListActivity {

	private static final int INSERT_REVIEW = 0;
	private static final int EDIT_REVIEW = 1;
	
	private ReviewDataSource datasource;
	List<ReviewBean> reviews;
	
	String parseMode;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_review_list);
		
		parseMode = getIntent().getStringExtra(ActivitySelection.PARSE_MODE);
		if (parseMode != null) datasource = new ParseReviewDataSource(this); 
		else datasource = new SQLiteReviewDataSource(this);
		datasource.open();
		
		fillData();
		
		registerForContextMenu(getListView());
	}
	
	public void fillData(){
//		String[] items = new String[] {"satu", "dua", "tiga", "empat"};
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.activity_list_row, R.id.text1, items);

		
		reviews = new ArrayList<ReviewBean>(datasource.getAllReviews());
//				List<ReviewBean>(datasource.getAllReviews());
		List<String> items = new ArrayList<String>();
		int counter = 0;
		for (ReviewBean bean : reviews) {
			String text = (++counter) + ". " + bean.getTitle() + " (" + bean.getReviewer() + ")";
			items.add(text);
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.activity_list_row, R.id.text1, items);
		
		//Toast.makeText(this, "Retrieved succeed with size : " + items.size(), Toast.LENGTH_SHORT).show();
		
		setListAdapter(adapter);
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id){
		super.onListItemClick(l, v, position, id);
		Intent i = new Intent(this, ReviewDisplayTest.class);
		i.putExtra(ReviewBean.REVIEW_TABLE_ID, reviews.get((int) id).getId());
		//Toast.makeText(this, "Populate Display Parse" + i.getExtras(), Toast.LENGTH_SHORT).show();
		if (parseMode != null) i.putExtra(ActivitySelection.PARSE_MODE, parseMode);
		startActivity(i);
	}
	
	@SuppressLint({ "InlinedApi", "NewApi" })
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Menu items default to never show in the action bar. On most devices this means
        // they will show in the standard options menu panel when the menu button is pressed.
        // On xlarge-screen devices a "More" button will appear in the far right of the
        // Action Bar that will display remaining items in a cascading menu.
        //menu.add("Normal item");

        MenuItem actionItem = menu.add("Add Button");
        

        // Items that show as actions should favor the "if room" setting, which will
        // prevent too many buttons from crowding the bar. Extra items will show in the
        // overflow area.
        //actionItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        actionItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        
        // Items that show as actions are strongly encouraged to use an icon.
        // These icons are shown without a text description, and therefore should
        // be sufficiently descriptive on their own.
        actionItem.setIcon(android.R.drawable.ic_menu_add);

        return true;
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		Intent i = new Intent(this, ReviewEditTest.class);
		switch (item.getItemId()) {
		case INSERT_REVIEW:
			if (parseMode != null) i.putExtra(ActivitySelection.PARSE_MODE, parseMode);
			startActivityForResult(i, INSERT_REVIEW);
			break;
		}
		return true;
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent){
		super.onActivityResult(requestCode, requestCode, intent);
		fillData();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo){
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu__review_list_longpress, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item){
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()){
		case R.id.delete_review:
			datasource.deleteReview(reviews.get((int) info.id).getId());
			fillData();
			break;
		case R.id.edit_review:
			Intent i = new Intent(this, ReviewEditTest.class);
			i.putExtra(ReviewBean.REVIEW_TABLE_ID, reviews.get((int) info.id).getId());
			if (parseMode != null) i.putExtra(ActivitySelection.PARSE_MODE, parseMode);
			startActivityForResult(i, EDIT_REVIEW);
			break;
		}
		return super.onContextItemSelected(item);
	}

}
