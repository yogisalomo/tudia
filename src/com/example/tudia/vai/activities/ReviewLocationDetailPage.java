package com.example.tudia.vai.activities;

import com.example.tudia.R;
import com.example.tudia.vai.storage.LocationBean;
import com.example.tudia.vai.storage.LocationDataSource;
import com.example.tudia.vai.storage.ReviewBean;
import com.example.tudia.vai.storage.ReviewDataSource;
import com.example.tudia.vai.storage.oncloud.ParseLocationDataSource;
import com.example.tudia.vai.storage.oncloud.ParseReviewDataSource;
import com.example.tudia.vai.storage.ondevice.SQLiteLocationDataSource;
import com.example.tudia.vai.storage.ondevice.SQLiteReviewDataSource;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class ReviewLocationDetailPage extends Fragment {

	TextView title;
	TextView city;
	TextView body;
	TextView ratingText;
	RatingBar ratingBar;
	
	String id;
	String parseMode;
	
	LocationBean bean;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		//super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.activity_location_display, container, false);
		//setContentView(R.layout.activity_review_display);
	
		parseMode = getActivity().getIntent().getStringExtra(ActivitySelection.PARSE_MODE);
		
		
		title = (TextView) view.findViewById(R.id.location_display_title);
		city = (TextView) view.findViewById(R.id.location_display_city);
		body = (TextView) view.findViewById(R.id.location_display_body);
		ratingText = (TextView) view.findViewById(R.id.location_display_rating_text);
		ratingBar = (RatingBar) view.findViewById(R.id.location_display_ratingbar);
		
		id = getActivity().getIntent().getStringExtra(LocationBean.LOCATION_TABLE_ID);
		if (id != null) populateFields();
		//else Toast.makeText(getActivity(), "No Populate Display Parse", Toast.LENGTH_SHORT).show();
		return view;
		
	}
	
	public void populateFields(){
		LocationDataSource datasource;
		if (parseMode != null){
			//Toast.makeText(getActivity(), "Populate Display Parse, ID" + id, Toast.LENGTH_SHORT).show();
			datasource = new ParseLocationDataSource(getActivity()); 
		}
		else{
			//Toast.makeText(getActivity(), "Populate Display SQL", Toast.LENGTH_SHORT).show();
			datasource = new SQLiteLocationDataSource(getActivity());
		}
		
		datasource.open();
		bean = datasource.get(id);
		if (bean == null) return;
		title.setText(bean.getTitle());
		city.setText(bean.getCity());
		body.setText(bean.getBody());
		ratingBar.setRating(bean.getRating());	
		ratingText.setText("Rating: " + ratingBar.getRating() + "/" + ratingBar.getNumStars());
		
		datasource.close();
	}
}
