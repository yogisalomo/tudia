package com.example.tudia.vai.activities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.example.tudia.R;
import com.example.tudia.vai.storage.LocationBean;
import com.example.tudia.vai.storage.ReviewBean;
import com.example.tudia.vai.utilities.ReviewGalleryImageAdapter;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.view.ActionProvider.VisibilityListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;

public class ReviewLocationGalleryPage extends Fragment {

	ImageView selectedImage;  
    private Integer[] mImageIds = {
               R.drawable.ciater,
               R.drawable.taman_bunga_cihideung,
               R.drawable.taman_wisata_maribaya,
               R.drawable.villa_istana_bunga,
               R.drawable.taman_lansia_bandung
       };
    
    private List<Bitmap> list;
    
    EditText name;
    Button upload;
    
    String location;
    ProgressDialog pd;
    //Bitmap bmp;
    int n;
   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
   {
       //super.onCreate(savedInstanceState);
       View view = inflater.inflate(R.layout.activity_review_location_gallery, container, false);
	   //setContentView(R.layout.activity_review_location_gallery);
      
       location = getActivity().getIntent().getStringExtra(LocationBean.LOCATION_TABLE_TITLE);
       populateList();
       
       name = (EditText) view.findViewById(R.id.loc_name);
       upload = (Button) view.findViewById(R.id.location_buttonUploadPicture);
      upload.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			uploadImage();
		}
	});
      
      //DEBUG
      name.setVisibility(View.INVISIBLE);
      upload.setVisibility(View.INVISIBLE);
      name.setEnabled(false);
      upload.setEnabled(false);
      
      
       Gallery gallery = (Gallery) view.findViewById(R.id.gallery1);
       selectedImage=(ImageView) view.findViewById(R.id.imageView1);
       gallery.setSpacing(1);
       gallery.setAdapter(new ReviewGalleryImageAdapter(getActivity(), list));
       
       // clicklistener for Gallery
       gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    	   public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
               //Toast.makeText(ReviewLocationGalleryPage.this.getActivity(), "Your selected position = " + position, Toast.LENGTH_SHORT).show();
               // show the selected Image
               selectedImage.setImageBitmap(list.get(position)); //setImageResource(mImageIds[position]);
           }
       });
       return view;
   }
   
   private void populateList(){
	   n = 0;
	   list = new ArrayList<Bitmap>();
	// TODO Auto-generated method stub
		//if (reviews != null) return reviews;
		
		
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("photos");
		
		
		query.whereEqualTo("name", location);
		query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
		
		pd = ProgressDialog.show(getActivity(), "Photos are being retrieved", "Patience. Be Right back");
		try{
			List<ParseObject> objects = query.find();
			for(ParseObject po: objects)
				{
					//ReviewBean puw = new ReviewBean(po);
					Bitmap bmp = decodeBitmap((ParseFile) po.get("ImageFile"));
					list.add(bmp);
				}
				//Toast.makeText(getActivity(), "Retrieve succeed with size : " + list.size(), Toast.LENGTH_SHORT).show();
			pd.cancel();
		} catch (ParseException e){
			e.printStackTrace();
			//Toast.makeText(getActivity(), "Retrieve failed", Toast.LENGTH_SHORT).show();
		}
		
	   
   }
   
   private Bitmap decodeBitmap(ParseFile fileObject){
	   Bitmap bmp = null;
	   try {
		   byte[] data = fileObject.getData();
		   try {
           	bmp = BitmapFactory.decodeByteArray( data, 0, data.length); 

               // Get the ImageView from 
               // main.xml 
              String root = Environment.getExternalStorageDirectory().toString();
               File myDir = new File(root + "/imagesuri1/");    


               String fname = "image-"+ n +".png";

              n++;
               File file = new File (myDir, fname);
              if (file.exists())
                  {file.delete();}

                      FileOutputStream out = new FileOutputStream(file);
                      bmp.compress(Bitmap.CompressFormat.PNG, 90, out);

                      out.flush();
                      out.close();  
               } catch (Exception b) {
                      b.printStackTrace();
               }

	   } catch (ParseException e){
		   //Toast.makeText(getActivity(), "Retrieve Failed", Toast.LENGTH_SHORT).show();
	   }
	   return bmp;
//	   fileObject.getDataInBackground(new GetDataCallback() {
//		
//		@Override
//		public void done(byte[] data, ParseException e) {
//			// TODO Auto-generated method stub
//			//
//			
//			    if (e == null) { 
//
//                    // Decode the Byte[] into 
//                    // Bitmap 
//                  try {
//                	bmp = BitmapFactory.decodeByteArray( data, 0, data.length); 
//
//                    // Get the ImageView from 
//                    // main.xml 
//                   String root = Environment.getExternalStorageDirectory().toString();
//                    File myDir = new File(root + "/imagesuri1/");    
//
//
//                    String fname = "image-"+ n +".png";
//
//                   n++;
//                    File file = new File (myDir, fname);
//                   if (file.exists())
//                       {file.delete();}
//
//                           FileOutputStream out = new FileOutputStream(file);
//                           bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
//
//                           out.flush();
//                           out.close();  
//                    } catch (Exception b) {
//                           b.printStackTrace();
//                    }
//                    // Close progress dialog 
//
//                    //progressDialog.dismiss();
//                } 
//            else { 
//         	   	Toast.makeText(getActivity(), "Retrieve Failed", Toast.LENGTH_SHORT).show();
//         
//            }
//
//			
//			//
//		}
//	});
   }
   
   private void uploadImage(){
	   Bitmap bitmap = ((BitmapDrawable) selectedImage.getDrawable()).getBitmap();//= BitmapFactory.decodeResource(getResources(),R.drawable.androidbegin);
       
	   if (bitmap == null) return;
   	
   	// Convert it to byte
       ByteArrayOutputStream stream = new ByteArrayOutputStream();
       // Compress image to lower quality scale 1 - 100
       //bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
       bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
       byte[] image = stream.toByteArray();

       // Create the ParseFile
       ParseFile file = new ParseFile("screenshot.png", image);
       // Upload the image into Parse Cloud
       file.saveInBackground();

       // Create a New Class called "ImageUpload" in Parse
       ParseObject imgupload = new ParseObject("photos");

       // Create a column named "ImageName" and set the string
       imgupload.put("name", name.getEditableText().toString());

       // Create a column named "ImageFile" and insert the image
       imgupload.put("ImageFile", file);

       // Create the class and the columns
       imgupload.saveInBackground();
       //Toast.makeText(getActivity(), "Image Uploaded", Toast.LENGTH_SHORT).show();
//       try{
//           imgupload.save();
//           // Show a simple toast message
//           Toast.makeText(ImageUploadTest.this, "Image Uploaded",
//                   Toast.LENGTH_SHORT).show();
//       } catch (ParseException e){
//       	Toast.makeText(ImageUploadTest.this, "Upload failed " + e.getMessage(),
//                   Toast.LENGTH_SHORT).show();
//       	e.printStackTrace();
//       }
   }

 }

