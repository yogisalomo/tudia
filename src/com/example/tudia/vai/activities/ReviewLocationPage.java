package com.example.tudia.vai.activities;


import com.example.tudia.R;
import com.example.tudia.vai.utilities.ReviewSectionsPagerAdapter;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

public class ReviewLocationPage extends FragmentActivity {

	ReviewSectionsPagerAdapter mSectionsPagerAdapter;

	static ViewPager mViewPager;

	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_page);
        
     // Create the adapter that will return a fragment for each of the three
	// primary sections of the app.
    mSectionsPagerAdapter = new ReviewSectionsPagerAdapter(getSupportFragmentManager());

	// Set up the ViewPager with the sections adapter.
	mViewPager = (ViewPager) findViewById(R.id.review_pager);
	mViewPager.setAdapter(mSectionsPagerAdapter);
	
	//mViewPager.setCurrentItem(1);
	//PagerTabStrip tab = (PagerTabStrip) findViewById(R.id.review_pager_tab_strip);
	//tab.setDrawFullUnderline(true);
	//tab.setTabIndicatorColor(Color.RED);
    }

}
