package com.example.tudia.vai.storage;

public interface Bean {
	public String getTableName();
	public String getTableDefinition();
}
