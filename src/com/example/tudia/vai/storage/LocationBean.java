package com.example.tudia.vai.storage;

import com.example.tudia.vai.storage.oncloud.ParseObjectWrapper;
import com.parse.ParseObject;

public class LocationBean extends ParseObjectWrapper implements Bean {

	public static final String LOCATION_TABLE_NAME = "locations";
	public static final String LOCATION_TABLE_ID = "_id";
	public static final String LOCATION_TABLE_TITLE = "title";
	public static final String LOCATION_TABLE_CITY = "city";
	public static final String LOCATION_TABLE_RATING = "rating";
	public static final String LOCATION_TABLE_BODY = "body";
	
	private final String LOCATION_TABLE_DEFINITION = " create table " +
			LOCATION_TABLE_NAME + " ( " 
			+ LOCATION_TABLE_ID + " integer primary key autoincrement, "
			+ LOCATION_TABLE_TITLE + " text not null, "
			+ LOCATION_TABLE_CITY + " text not null, "
			+ LOCATION_TABLE_RATING + " text not null, "
			+ LOCATION_TABLE_BODY + " text not null " 
			+ ");";
	
	private String id;
	private String title;
	private String city;
	private float rating;
	private String body;
	
	
	public LocationBean() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	//Parse constructor
	public LocationBean(String _title, String _city,  float _rating, String _body){
		super(LOCATION_TABLE_NAME);
		//setId(po.getObjectId());
		setTitle(_title);
		setCity(_city);
		setRating(_rating);
		setBody(_body);
	}
	
	public LocationBean(ParseObject po){
		super(po);
	}
	
	public String getId(){
		if (po == null) return id;
		else return po.getObjectId();
	}
	
	public void setId(String _id){
		id = _id;
	}
	
	public String getTitle(){
		if (po == null) return title;
		else return po.getString(LOCATION_TABLE_TITLE);
	}
	
	public void setTitle(String _title){
		if (po == null)	title = _title;
		else po.put(LOCATION_TABLE_TITLE, _title);
	}
	
	public String getCity(){
		if (po == null) return city;
		else return po.getString(LOCATION_TABLE_CITY);
	}
	
	public void setCity(String _city){
		if (po == null)	city = _city;
		else po.put(LOCATION_TABLE_CITY, _city);
	}
	
	public float getRating(){
		if (po == null) return rating;
		else return (float) po.getDouble(LOCATION_TABLE_RATING);
	}
	
	public void setRating(float _rating){
		if (po == null) rating = _rating;
		else po.put(LOCATION_TABLE_RATING, (double) _rating);
	}
	
	public String getBody(){
		if (po == null) return body;
		else return po.getString(LOCATION_TABLE_BODY);
	}
	
	public void setBody(String _body){
		if (po == null) body = _body;
		else po.put(LOCATION_TABLE_BODY, _body);
	}
	
	public String getTableDefinition(){
		return LOCATION_TABLE_DEFINITION;
	}
	
	public String getTableName(){
		return LOCATION_TABLE_NAME;
	}

}
