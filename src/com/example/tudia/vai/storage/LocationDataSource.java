package com.example.tudia.vai.storage;

import java.util.List;

public interface LocationDataSource {
	public void open();
	
	public void close();
	
	public void insert(LocationBean bean);
	
	public void delete(String id);
	
	public boolean update(LocationBean bean);
	
	public List<LocationBean> getAll();
	
	public LocationBean get(String id);
	
	public List<LocationBean> getAllWhereClause(String[] args, String[] vals);
	
}
