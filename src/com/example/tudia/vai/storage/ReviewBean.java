package com.example.tudia.vai.storage;

import com.example.tudia.vai.storage.oncloud.ParseObjectWrapper;
import com.parse.ParseObject;

public class ReviewBean extends ParseObjectWrapper implements Bean {

	public static final String REVIEW_TABLE_NAME = "reviews";
	public static final String REVIEW_TABLE_ID = "_id";
	public static final String REVIEW_TABLE_TITLE = "title";
	public static final String REVIEW_TABLE_CITY = "city";
	public static final String REVIEW_TABLE_REVIEWER = "reviewer";
	public static final String REVIEW_TABLE_RATING = "rating";
	public static final String REVIEW_TABLE_BODY = "body";
	
	private final String REVIEW_TABLE_DEFINITION = " create table " +
			REVIEW_TABLE_NAME + " ( " 
			+ REVIEW_TABLE_ID + " integer primary key autoincrement, "
			+ REVIEW_TABLE_TITLE + " text not null, "
			+ REVIEW_TABLE_CITY + " text not null, "
			+ REVIEW_TABLE_REVIEWER + " text not null, "
			+ REVIEW_TABLE_RATING + " text not null, "
			+ REVIEW_TABLE_BODY + " text not null " 
			+ ");";
	
	private String id;
	private String title;
	private String city;
	private String reviewer;
	private float rating;
	private String body;
	
	
	public ReviewBean() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	//Parse constructor
	public ReviewBean(String _title, String _city, String _reviewer, float _rating, String _body){
		super(REVIEW_TABLE_NAME);
		//setId(po.getObjectId());
		setTitle(_title);
		setCity(_city);
		setReviewer(_reviewer);
		setRating(_rating);
		setBody(_body);
	}
	
	public ReviewBean(ParseObject po){
		super(po);
	}
	
	public String getId(){
		if (po == null) return id;
		else return po.getObjectId();
	}
	
	public void setId(String _id){
		id = _id;
	}
	
	public String getTitle(){
		if (po == null) return title;
		else return po.getString(REVIEW_TABLE_TITLE);
	}
	
	public void setTitle(String _title){
		if (po == null)	title = _title;
		else po.put(REVIEW_TABLE_TITLE, _title);
	}
	
	public String getCity(){
		if (po == null) return city;
		else return po.getString(REVIEW_TABLE_CITY);
	}
	
	public void setCity(String _city){
		if (po == null)	city = _city;
		else po.put(REVIEW_TABLE_CITY, _city);
	}
	
	public String getReviewer(){
		if (po == null) return reviewer;
		else return po.getString(REVIEW_TABLE_REVIEWER);
	}
	
	public void setReviewer(String _reviewer){
		if (po == null) reviewer = _reviewer;
		else po.put(REVIEW_TABLE_REVIEWER, _reviewer);
	}
	
	public float getRating(){
		if (po == null) return rating;
		else return (float) po.getDouble(REVIEW_TABLE_RATING);
	}
	
	public void setRating(float _rating){
		if (po == null) rating = _rating;
		else po.put(REVIEW_TABLE_RATING, (double) _rating);
	}
	
	public String getBody(){
		if (po == null) return body;
		else return po.getString(REVIEW_TABLE_BODY);
	}
	
	public void setBody(String _body){
		if (po == null) body = _body;
		else po.put(REVIEW_TABLE_BODY, _body);
	}
	
	public String getTableDefinition(){
		return REVIEW_TABLE_DEFINITION;
	}
	
	public String getTableName(){
		return REVIEW_TABLE_NAME;
	}

}
