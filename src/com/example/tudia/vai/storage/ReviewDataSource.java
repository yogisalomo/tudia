package com.example.tudia.vai.storage;

import java.util.List;



public interface ReviewDataSource {
	
	
	public void open();
	
	public void close();
	
	public void insertReview(ReviewBean bean);
	
	public void deleteReview(String id);
	
	public boolean updateReview(ReviewBean bean);
	
	public List<ReviewBean> getAllReviews();
	
	public ReviewBean getReview(String id);
	
	public List<ReviewBean> getAllWhereClause(String[] args, String[] vals);
	

}
