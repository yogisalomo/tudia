package com.example.tudia.vai.storage.oncloud;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.tudia.vai.storage.LocationBean;
import com.example.tudia.vai.storage.LocationDataSource;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ParseLocationDataSource implements LocationDataSource {

	private Context context;
	ProgressDialog pd;
	
	static List<LocationBean> locations;
	
	public ParseLocationDataSource(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		
		
	}

	@Override
	public void open() {
		// TODO Auto-generated method stub
		//Parse.initialize(context, "OgnRLUxKy4hAmFczNpvyLRM3EwYi9Vmacu87tTpn", "qTo7hpVe6ixS1BGIkUGcuN6kua7ZRUM1y6hM6bSW");
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
	}

	@Override
	public void insert(LocationBean bean) {
		// TODO Auto-generated method stub
		pd = ProgressDialog.show(context, "Uploading Location Info", "Please wait.");
		try{
			bean.po.save();
			Toast.makeText(context, "Insert succeed.", Toast.LENGTH_SHORT).show();
		} catch (ParseException e){
			e.printStackTrace();
			Toast.makeText(context, "Insert failed", Toast.LENGTH_SHORT).show();
		}
		pd.cancel();
		
//		bean.po.saveInBackground(new SaveCallback() {
//			
//			@Override
//			public void done(ParseException arg0) {
//				// TODO Auto-generated method stub
//				if (pd.isShowing()) pd.cancel();
//				if (arg0 == null){
//				} else Toast.makeText(context, "Insert failed.", Toast.LENGTH_SHORT).show();
//			}
//		});
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		LocationBean bean = get(id);
		try{
			bean.po.delete();
			Toast.makeText(context, "Delete succeed.", Toast.LENGTH_SHORT).show();
		} catch (ParseException e){
			e.printStackTrace();
			Toast.makeText(context, "Delete failed", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean update(LocationBean bean) {
		// TODO Auto-generated method stub
		Toast.makeText(context, "Update ID : " + bean.getId(), Toast.LENGTH_SHORT).show();
		try{
			bean.po.save();
			Toast.makeText(context, "Update succeed.", Toast.LENGTH_SHORT).show();
		} catch (ParseException e){
			e.printStackTrace();
			Toast.makeText(context, "Update failed", Toast.LENGTH_SHORT).show();
		}
		Log.d("EDIT", "ID2 : " + bean.getId());
		return true;
	}

	@Override
	public List<LocationBean> getAll() {
		// TODO Auto-generated method stub
		//if (reviews != null) return reviews;
		locations = new ArrayList<LocationBean>();
		
		
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(LocationBean.LOCATION_TABLE_NAME);
		//query.whereEqualTo("city", "Jakarta");
		query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
		//query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
		//Milliseconds
		//query.setMaxCacheAge(100000L);
		
		
		pd = ProgressDialog.show(context, "Location Infos are being retrieved", "Patience. Be Right back");
		try{
			List<ParseObject> objects = query.find();
			for(ParseObject po: objects)
				{
					LocationBean puw = new LocationBean(po);
					locations.add(puw);
				}
				Toast.makeText(context, "Retrieve succeed with location infos size : " + locations.size(), Toast.LENGTH_SHORT).show();
			pd.cancel();
		} catch (ParseException e){
			e.printStackTrace();
			Toast.makeText(context, "Retrieve failed", Toast.LENGTH_SHORT).show();
		}
		
//		query.findInBackground(new FindCallback<ParseObject>() {
//			
//			@Override
//			public void done(List<ParseObject> objects, ParseException arg1) {
//				// TODO Auto-generated method stub
//				pd.cancel();
//				pd = null;
//				if (arg1 == null) {
//					
//					for(ParseObject po: objects)
//					{
//						ReviewBean puw = new ReviewBean(po);
//						reviews.add(puw);
//					}
//					Toast.makeText(context, "Retrieved succeed with review size : " + reviews.size(), Toast.LENGTH_SHORT).show();
//				} else {
//					arg1.printStackTrace();
//					Toast.makeText(context, "Retrieved failed", Toast.LENGTH_SHORT).show();
//				}
//			}
//		});
		return locations;
	}

	@Override
	public LocationBean get(String id) {
		// TODO Auto-generated method stub
//		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(TudiaSQLiteHelper.REVIEW_TABLE_NAME);
//		query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
//		//query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
//		//Milliseconds
//		//query.setMaxCacheAge(100000L);
//		ReviewBean bean = null;
//		pd = ProgressDialog.show(context, "Retrieving review", "Patience. Be Right back");
//		try{
//			bean = new ReviewBean(query.get(String.valueOf(id)));
//			Toast.makeText(context, "Retrieve succeed", Toast.LENGTH_SHORT).show();
//			pd.cancel();
//		} catch (ParseException e){
//			Toast.makeText(context, "Retrieve failed", Toast.LENGTH_SHORT).show();
//			e.printStackTrace();
//			
//		}
		//Toast.makeText(context, "Retrieve id " + id + reviews, Toast.LENGTH_SHORT).show();
		LocationBean bean = null;
		if (locations == null) return null;//reviews = getAllReviews();
		for (LocationBean b : locations){
			if (b.getId().contentEquals(id)){
				bean = b;
				break;
			}
		}
		return bean;
	}
	
	@Override
	public List<LocationBean> getAllWhereClause(String[] args, String[] vals){
		// TODO Auto-generated method stub
				//if (reviews != null) return reviews;
				locations = new ArrayList<LocationBean>();
				
				
				ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(LocationBean.LOCATION_TABLE_NAME);
				for (int i = 0; i < args.length ; i++) {
					query.whereEqualTo(args[i], vals[i]);
				}
				
				query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
				//query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
				//Milliseconds
				//query.setMaxCacheAge(100000L);
				
				
				pd = ProgressDialog.show(context, "Location Infos are being retrieved", "Patience. Be Right back");
				try{
					List<ParseObject> objects = query.find();
					for(ParseObject po: objects)
						{
							LocationBean puw = new LocationBean(po);
							locations.add(puw);
						}
						//Toast.makeText(context, "Retrieve succeed with location infos size : " + locations.size(), Toast.LENGTH_SHORT).show();
					pd.cancel();
				} catch (ParseException e){
					e.printStackTrace();
					//Toast.makeText(context, "Retrieve failed", Toast.LENGTH_SHORT).show();
				}
				
//				query.findInBackground(new FindCallback<ParseObject>() {
//					
//					@Override
//					public void done(List<ParseObject> objects, ParseException arg1) {
//						// TODO Auto-generated method stub
//						pd.cancel();
//						pd = null;
//						if (arg1 == null) {
//							
//							for(ParseObject po: objects)
//							{
//								ReviewBean puw = new ReviewBean(po);
//								reviews.add(puw);
//							}
//							Toast.makeText(context, "Retrieved succeed with review size : " + reviews.size(), Toast.LENGTH_SHORT).show();
//						} else {
//							arg1.printStackTrace();
//							Toast.makeText(context, "Retrieved failed", Toast.LENGTH_SHORT).show();
//						}
//					}
//				});
				return locations;	
	}

}
