package com.example.tudia.vai.storage.oncloud;

import com.parse.ParseObject;

public class ParseObjectWrapper {

	
	public ParseObject po;
	
	public ParseObjectWrapper(){
		po = null;
	}
	
	public ParseObjectWrapper(String tablename){
		po = new ParseObject(tablename);
	}
	public ParseObjectWrapper(ParseObject in){
		po = in;
	}
	
	//Accessors
	public ParseObject getParseObject() { return po; }
	
	String getTablename(){
		return po.getClassName();
	}
	
	
}
