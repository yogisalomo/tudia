package com.example.tudia.vai.storage.oncloud;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.tudia.vai.storage.ReviewBean;
import com.example.tudia.vai.storage.ReviewDataSource;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ParseReviewDataSource implements ReviewDataSource {

	//private static ParseReviewDataSource instance;
	
	private Context context;
	ProgressDialog pd;
	
	static List<ReviewBean> reviews;
	
	public ParseReviewDataSource(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		
		
	}

	@Override
	public void open() {
		// TODO Auto-generated method stub
		//Parse.initialize(context, "OgnRLUxKy4hAmFczNpvyLRM3EwYi9Vmacu87tTpn", "qTo7hpVe6ixS1BGIkUGcuN6kua7ZRUM1y6hM6bSW");
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
	}

	@Override
	public void insertReview(ReviewBean bean) {
		// TODO Auto-generated method stub
		pd = ProgressDialog.show(context, "Uploading Review", "Please wait.");
		try{
			bean.po.save();
			Toast.makeText(context, "Insert succeed.", Toast.LENGTH_SHORT).show();
		} catch (ParseException e){
			e.printStackTrace();
			Toast.makeText(context, "Insert failed", Toast.LENGTH_SHORT).show();
		}
		pd.cancel();
		
//		bean.po.saveInBackground(new SaveCallback() {
//			
//			@Override
//			public void done(ParseException arg0) {
//				// TODO Auto-generated method stub
//				if (pd.isShowing()) pd.cancel();
//				if (arg0 == null){
//				} else Toast.makeText(context, "Insert failed.", Toast.LENGTH_SHORT).show();
//			}
//		});
	}

	@Override
	public void deleteReview(String id) {
		// TODO Auto-generated method stub
		ReviewBean bean = getReview(id);
		try{
			bean.po.delete();
			Toast.makeText(context, "Delete succeed.", Toast.LENGTH_SHORT).show();
		} catch (ParseException e){
			e.printStackTrace();
			Toast.makeText(context, "Delete failed", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean updateReview(ReviewBean bean) {
		// TODO Auto-generated method stub
		Toast.makeText(context, "Update ID : " + bean.getId(), Toast.LENGTH_SHORT).show();
		try{
			bean.po.save();
			Toast.makeText(context, "Update succeed.", Toast.LENGTH_SHORT).show();
		} catch (ParseException e){
			e.printStackTrace();
			Toast.makeText(context, "Update failed", Toast.LENGTH_SHORT).show();
		}
		Log.d("EDIT", "ID2 : " + bean.getId());
		return true;
	}

	@Override
	public List<ReviewBean> getAllReviews() {
		// TODO Auto-generated method stub
		//if (reviews != null) return reviews;
		reviews = new ArrayList<ReviewBean>();
		
		
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ReviewBean.REVIEW_TABLE_NAME);
		//query.whereEqualTo("city", "Jakarta");
		query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
		//query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
		//Milliseconds
		//query.setMaxCacheAge(100000L);
		
		
		pd = ProgressDialog.show(context, "Reviews are being retrieved", "Patience. Be Right back");
		try{
			List<ParseObject> objects = query.find();
			for(ParseObject po: objects)
				{
					ReviewBean puw = new ReviewBean(po);
					reviews.add(puw);
				}
				Toast.makeText(context, "Retrieve succeed with review size : " + reviews.size(), Toast.LENGTH_SHORT).show();
			pd.cancel();
		} catch (ParseException e){
			e.printStackTrace();
			Toast.makeText(context, "Retrieve failed", Toast.LENGTH_SHORT).show();
		}
		
//		query.findInBackground(new FindCallback<ParseObject>() {
//			
//			@Override
//			public void done(List<ParseObject> objects, ParseException arg1) {
//				// TODO Auto-generated method stub
//				pd.cancel();
//				pd = null;
//				if (arg1 == null) {
//					
//					for(ParseObject po: objects)
//					{
//						ReviewBean puw = new ReviewBean(po);
//						reviews.add(puw);
//					}
//					Toast.makeText(context, "Retrieved succeed with review size : " + reviews.size(), Toast.LENGTH_SHORT).show();
//				} else {
//					arg1.printStackTrace();
//					Toast.makeText(context, "Retrieved failed", Toast.LENGTH_SHORT).show();
//				}
//			}
//		});
		return reviews;
	}

	@Override
	public ReviewBean getReview(String id) {
		// TODO Auto-generated method stub
//		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(TudiaSQLiteHelper.REVIEW_TABLE_NAME);
//		query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
//		//query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
//		//Milliseconds
//		//query.setMaxCacheAge(100000L);
//		ReviewBean bean = null;
//		pd = ProgressDialog.show(context, "Retrieving review", "Patience. Be Right back");
//		try{
//			bean = new ReviewBean(query.get(String.valueOf(id)));
//			Toast.makeText(context, "Retrieve succeed", Toast.LENGTH_SHORT).show();
//			pd.cancel();
//		} catch (ParseException e){
//			Toast.makeText(context, "Retrieve failed", Toast.LENGTH_SHORT).show();
//			e.printStackTrace();
//			
//		}
		//Toast.makeText(context, "Retrieve id " + id + reviews, Toast.LENGTH_SHORT).show();
		ReviewBean bean = null;
		if (reviews == null) return null;//reviews = getAllReviews();
		for (ReviewBean b : reviews){
			if (b.getId().contentEquals(id)){
				bean = b;
				break;
			}
		}
		return bean;
	}
	
	@Override
	public List<ReviewBean> getAllWhereClause(String[] args, String[] vals){
		
		// TODO Auto-generated method stub
				//if (reviews != null) return reviews;
				reviews = new ArrayList<ReviewBean>();
				
				
				ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ReviewBean.REVIEW_TABLE_NAME);
				
				for (int i = 0; i < args.length ; i++) {
					query.whereEqualTo(args[i], vals[i]);
				}
				
				//query.whereEqualTo("city", "Jakarta");
				query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
				//query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
				//Milliseconds
				//query.setMaxCacheAge(100000L);
				
				
				pd = ProgressDialog.show(context, "Reviews are being retrieved", "Patience. Be Right back");
				try{
					List<ParseObject> objects = query.find();
					for(ParseObject po: objects)
						{
							ReviewBean puw = new ReviewBean(po);
							reviews.add(puw);
						}
						Toast.makeText(context, "Retrieve succeed with review size : " + reviews.size(), Toast.LENGTH_SHORT).show();
					pd.cancel();
				} catch (ParseException e){
					e.printStackTrace();
					Toast.makeText(context, "Retrieve failed", Toast.LENGTH_SHORT).show();
				}
				
//				query.findInBackground(new FindCallback<ParseObject>() {
//					
//					@Override
//					public void done(List<ParseObject> objects, ParseException arg1) {
//						// TODO Auto-generated method stub
//						pd.cancel();
//						pd = null;
//						if (arg1 == null) {
//							
//							for(ParseObject po: objects)
//							{
//								ReviewBean puw = new ReviewBean(po);
//								reviews.add(puw);
//							}
//							Toast.makeText(context, "Retrieved succeed with review size : " + reviews.size(), Toast.LENGTH_SHORT).show();
//						} else {
//							arg1.printStackTrace();
//							Toast.makeText(context, "Retrieved failed", Toast.LENGTH_SHORT).show();
//						}
//					}
//				});
				return reviews;
		
	}

}
