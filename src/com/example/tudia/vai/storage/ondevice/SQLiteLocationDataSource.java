package com.example.tudia.vai.storage.ondevice;

//NOTE : change data source implementation as generic as possible

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.example.tudia.vai.storage.LocationBean;
import com.example.tudia.vai.storage.LocationDataSource;

public class SQLiteLocationDataSource implements LocationDataSource {

	private SQLiteDatabase db;
	private TudiaSQLiteHelper locationHelper;
	private String[] fieldNames = {LocationBean.LOCATION_TABLE_ID, 
			LocationBean.LOCATION_TABLE_TITLE,
			LocationBean.LOCATION_TABLE_CITY,
			LocationBean.LOCATION_TABLE_RATING,
			LocationBean.LOCATION_TABLE_BODY};
	
	public SQLiteLocationDataSource(Context context) {
		// TODO Auto-generated constructor stub
		locationHelper = new TudiaSQLiteHelper(context, LocationBean.class);
	}
	
	@Override
	public void open() throws SQLiteException{
		db = locationHelper.getWritableDatabase();
	}
	
	@Override
	public void close(){
		locationHelper.close();
	}
	
	@Override
	public void insert(LocationBean bean){
		ContentValues values = new ContentValues();
		values.put(fieldNames[1], bean.getTitle());
		values.put(fieldNames[2], bean.getCity());
		values.put(fieldNames[3], bean.getRating());
		values.put(fieldNames[4], bean.getBody());
		long id = db.insert(LocationBean.LOCATION_TABLE_NAME, null, values);
		bean.setId(String.valueOf(id));
	}
	
	@Override
	public void delete(String id){
		db.delete(LocationBean.LOCATION_TABLE_NAME, LocationBean.LOCATION_TABLE_ID +" = " + id, null);
	}
	
	@Override
	public boolean update(LocationBean bean){
		ContentValues values  = new ContentValues();
		values.put(fieldNames[1], bean.getTitle());
		values.put(fieldNames[2], bean.getCity());
		values.put(fieldNames[3], bean.getRating());
		values.put(fieldNames[4], bean.getBody());
		return db.update(LocationBean.LOCATION_TABLE_NAME, values, LocationBean.LOCATION_TABLE_ID +" = " + bean.getId(), null) > 0;
	}
	
	@Override
	public List<LocationBean> getAll(){
		List<LocationBean> locations = new ArrayList<LocationBean>();
		Cursor cursor = db.query(LocationBean.LOCATION_TABLE_NAME, fieldNames, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			LocationBean bean = new LocationBean();
			bean.setId(String.valueOf(cursor.getLong(0)));
			bean.setTitle(cursor.getString(1));
			bean.setCity(cursor.getString(2));
			bean.setRating(cursor.getFloat(3));
			bean.setBody(cursor.getString(4));
			locations.add(bean);
			cursor.moveToNext();
		}
		cursor.close();
		return locations;
	}
	
	public Cursor getAllCursor(){
		Cursor cursor = db.query(LocationBean.LOCATION_TABLE_NAME, fieldNames, null, null, null, null, null);
		return cursor;
	}
	
	@Override
	public LocationBean get(String id){
		Cursor cursor = db.query(LocationBean.LOCATION_TABLE_NAME, fieldNames, LocationBean.LOCATION_TABLE_ID + " = " + id, null, null, null, null);
		if (cursor != null){
			cursor.moveToFirst();
		}
		LocationBean bean = new LocationBean();
		bean.setId(String.valueOf(cursor.getLong(0)));
		bean.setTitle(cursor.getString(1));
		bean.setCity(cursor.getString(2));
		bean.setRating(cursor.getFloat(3));
		bean.setBody(cursor.getString(4));
		return bean;
	}
	
	public Cursor getCursor(long id){
		Cursor cursor = db.query(LocationBean.LOCATION_TABLE_NAME, fieldNames, LocationBean.LOCATION_TABLE_ID + " = " + id, null, null, null, null);
		if (cursor != null){
			cursor.moveToFirst();
		}
		return cursor;
	}
	
	@Override
	public List<LocationBean> getAllWhereClause(String[] args, String[] vals){
		List<LocationBean> locations = new ArrayList<LocationBean>();
		Cursor cursor = db.query(LocationBean.LOCATION_TABLE_NAME, fieldNames, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			LocationBean bean = new LocationBean();
			bean.setId(String.valueOf(cursor.getLong(0)));
			bean.setTitle(cursor.getString(1));
			bean.setCity(cursor.getString(2));
			bean.setRating(cursor.getFloat(3));
			bean.setBody(cursor.getString(4));
			locations.add(bean);
			cursor.moveToNext();
		}
		cursor.close();
		return locations;	
	}

}
