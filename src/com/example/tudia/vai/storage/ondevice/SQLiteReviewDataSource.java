package com.example.tudia.vai.storage.ondevice;

import java.util.ArrayList;
import java.util.List;

import com.example.tudia.vai.storage.ReviewBean;
import com.example.tudia.vai.storage.ReviewDataSource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class SQLiteReviewDataSource implements ReviewDataSource {

	//TODO:
	//update review
	
	private SQLiteDatabase db;
	private TudiaSQLiteHelper reviewHelper;
	private String[] fieldNames = {ReviewBean.REVIEW_TABLE_ID, 
			ReviewBean.REVIEW_TABLE_TITLE,
			ReviewBean.REVIEW_TABLE_CITY,
			ReviewBean.REVIEW_TABLE_REVIEWER,
			ReviewBean.REVIEW_TABLE_RATING,
			ReviewBean.REVIEW_TABLE_BODY};
	
	public SQLiteReviewDataSource(Context context) {
		// TODO Auto-generated constructor stub
		reviewHelper = new TudiaSQLiteHelper(context, ReviewBean.class);
	}
	
	@Override
	public void open() throws SQLiteException{
		db = reviewHelper.getWritableDatabase();
	}
	
	@Override
	public void close(){
		reviewHelper.close();
	}
	
	@Override
	public void insertReview(ReviewBean bean){
		ContentValues values = new ContentValues();
		values.put(fieldNames[1], bean.getTitle());
		values.put(fieldNames[2], bean.getCity());
		values.put(fieldNames[3], bean.getReviewer());
		values.put(fieldNames[4], bean.getRating());
		values.put(fieldNames[5], bean.getBody());
		long id = db.insert(ReviewBean.REVIEW_TABLE_NAME, null, values);
		bean.setId(String.valueOf(id));
	}
	
	@Override
	public void deleteReview(String id){
		db.delete(ReviewBean.REVIEW_TABLE_NAME, ReviewBean.REVIEW_TABLE_ID +" = " + id, null);
	}
	
	@Override
	public boolean updateReview(ReviewBean bean){
		ContentValues values  = new ContentValues();
		values.put(fieldNames[1], bean.getTitle());
		values.put(fieldNames[2], bean.getCity());
		values.put(fieldNames[3], bean.getReviewer());
		values.put(fieldNames[4], bean.getRating());
		values.put(fieldNames[5], bean.getBody());
		return db.update(ReviewBean.REVIEW_TABLE_NAME, values, ReviewBean.REVIEW_TABLE_ID +" = " + bean.getId(), null) > 0;
	}
	
	@Override
	public List<ReviewBean> getAllReviews(){
		List<ReviewBean> reviews = new ArrayList<ReviewBean>();
		Cursor cursor = db.query(ReviewBean.REVIEW_TABLE_NAME, fieldNames, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			ReviewBean bean = new ReviewBean();
			bean.setId(String.valueOf(cursor.getLong(0)));
			bean.setTitle(cursor.getString(1));
			bean.setCity(cursor.getString(2));
			bean.setReviewer(cursor.getString(3));
			bean.setRating(cursor.getFloat(4));
			bean.setBody(cursor.getString(5));
			reviews.add(bean);
			cursor.moveToNext();
		}
		cursor.close();
		return reviews;
	}
	
	public Cursor getAllReviewsCursor(){
		Cursor cursor = db.query(ReviewBean.REVIEW_TABLE_NAME, fieldNames, null, null, null, null, null);
		return cursor;
	}
	
	@Override
	public ReviewBean getReview(String id){
		Cursor cursor = db.query(ReviewBean.REVIEW_TABLE_NAME, fieldNames, ReviewBean.REVIEW_TABLE_ID + " = " + id, null, null, null, null);
		if (cursor != null){
			cursor.moveToFirst();
		}
		ReviewBean bean = new ReviewBean();
		bean.setId(String.valueOf(cursor.getLong(0)));
		bean.setTitle(cursor.getString(1));
		bean.setCity(cursor.getString(2));
		bean.setReviewer(cursor.getString(3));
		bean.setRating(cursor.getFloat(4));
		bean.setBody(cursor.getString(5));
		return bean;
	}
	
	public Cursor getReviewCursor(long id){
		Cursor cursor = db.query(ReviewBean.REVIEW_TABLE_NAME, fieldNames, ReviewBean.REVIEW_TABLE_ID + " = " + id, null, null, null, null);
		if (cursor != null){
			cursor.moveToFirst();
		}
		return cursor;
	}
	
	public List<ReviewBean> getAllWhereClause(String[] args, String[] vals){
		List<ReviewBean> reviews = new ArrayList<ReviewBean>();
		//String arg = 
		Cursor cursor = db.query(ReviewBean.REVIEW_TABLE_NAME, fieldNames, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			ReviewBean bean = new ReviewBean();
			bean.setId(String.valueOf(cursor.getLong(0)));
			bean.setTitle(cursor.getString(1));
			bean.setCity(cursor.getString(2));
			bean.setReviewer(cursor.getString(3));
			bean.setRating(cursor.getFloat(4));
			bean.setBody(cursor.getString(5));
			reviews.add(bean);
			cursor.moveToNext();
		}
		cursor.close();
		return reviews;
	}

}
