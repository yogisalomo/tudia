package com.example.tudia.vai.storage.ondevice;

import com.example.tudia.vai.storage.Bean;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class TudiaSQLiteHelper extends SQLiteOpenHelper {

	
	
	private static final String DATABASE_NAME = "tudia.db";
	private static final int DATABASE_VERSION = 4;
	
	private Bean tableClass;
	
	public TudiaSQLiteHelper(Context context, Class<? extends Bean> className){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		try{
			tableClass = className.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public TudiaSQLiteHelper(Context context, String name,
			CursorFactory factory, int version, Class<Bean> className) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
		try{
			tableClass = className.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

		@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
			db.execSQL(tableClass.getTableDefinition());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("drop table if exists " + tableClass.getTableName());
		onCreate(db);
	}

}
