package com.example.tudia.vai.utilities;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.TextView;

public class DatePickHandler implements OnClickListener {
	
	public static final int DATE_DIALOG_ID = 0;
	
	private Context context;
	
	// where we display the selected date and time
    private TextView observer = null;
    private CharSequence observerText;
    
    // date and time
    private int mYear;
    private int mMonth;
    private int mDay;
    
	public DatePickHandler(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		init();
	}
	
	public DatePickHandler(Context context, TextView observer) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.observer = observer;
		init();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (context instanceof Activity) ((Activity) context).showDialog(DATE_DIALOG_ID);
	}
	
	private void init(){
	    final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
   
        if (observer != null) updateDisplay();
	}
	
	public Dialog createDatePickerDialog(){
		return new DatePickerDialog(context, mDateSetListener, mYear, mMonth, mDay);
	}
	
	public void prepareDatePickerDialog(Dialog dialog){
		((DatePickerDialog) dialog).updateDate(mYear, mMonth, mDay);
	}
	
	public void setObserver(TextView observer){
		this.observer = observer;
	}
	
	private void updateDisplay() {
		observerText = new StringBuilder()
        // Month is 0 based so add 1
        .append(mMonth + 1).append("-")
        .append(mDay).append("-")
        .append(mYear);
        observer.setText(observerText);
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear,
                        int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    updateDisplay();
                }
    };

    

}
