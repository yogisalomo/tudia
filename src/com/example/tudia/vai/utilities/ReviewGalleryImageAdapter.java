package com.example.tudia.vai.utilities;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class ReviewGalleryImageAdapter extends BaseAdapter {

	private Context mContext;

    private Integer[] mImageIds;
    private List<Bitmap> imageList;
    
    
    public ReviewGalleryImageAdapter(Context context, List<Bitmap> list) 
    {
        mContext = context;
        imageList = list;
    }

    public int getCount() {
        return imageList.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }


    // Override this method according to your need
    public View getView(int index, View view, ViewGroup viewGroup) 
    {
        // TODO Auto-generated method stub
        ImageView i = new ImageView(mContext);

        i.setImageBitmap(imageList.get(index));
        i.setLayoutParams(new Gallery.LayoutParams(200, 200));
    
        i.setScaleType(ImageView.ScaleType.FIT_XY);

        return i;
    }

}
