package com.example.tudia.vai.utilities;


import com.example.tudia.vai.activities.ReviewLocationDetailPage;
import com.example.tudia.vai.activities.ReviewLocationGalleryPage;
import com.example.tudia.vai.activities.ReviewLocationListPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ReviewSectionsPagerAdapter extends FragmentPagerAdapter {

	private Fragment[] fragments;
	public ReviewSectionsPagerAdapter(FragmentManager fm) {
		super(fm);
		fragments = new Fragment[3];
		fragments[0] = new ReviewLocationDetailPage();
		fragments[1] = new ReviewLocationListPage();
		fragments[2] = new ReviewLocationGalleryPage();
	}

	@Override
	public Fragment getItem(int position) {
		// getItem is called to instantiate the fragment for the given page.
		// Return a DummySectionFragment (defined as a static inner class
		// below) with the page number as its lone argument.
		
		if (position >= fragments.length) return null;
		Fragment fragment = fragments[position];// = new DummySectionFragment();
		
		
		
//		switch (position) {
//		case 0:
//			fragment = new ReviewLocationDetailPage();
//			break;
//		case 1:
//			fragment = new ReviewLocationListPage();
//			break;
//		case 2:
//			fragment = new ReviewLocationGalleryPage();
//			break;
//		default:
//			return null;
//		}
		
		Bundle args = new Bundle();
		//args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return 3;
	}

	@Override
	public CharSequence getPageTitle(int position) {
//		Locale l = Locale.getDefault();
		switch (position) {
		case 0:
			return "Detail";//getString(R.string.title_section1).toUpperCase(l);
		case 1:
			return "List";//getString(R.string.title_section2).toUpperCase(l);
		case 2:
			return "Gallery";
		}
		return null;
	}
}


