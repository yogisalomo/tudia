package com.example.tudia.vai.utilities;

import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.TimePicker;

public class TimePickHandler implements OnClickListener {

	public static final int TIME_DIALOG_ID = 1;
	
	private Context context;
	
	// where we display the selected date and time
    private TextView observer = null;
    private CharSequence observerText;
    
    // date and time
    private int mHour;
    private int mMinute;
    
    public TimePickHandler(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		init();
	}
	
	public TimePickHandler(Context context, TextView observer) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.observer = observer;
		init();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (context instanceof Activity) ((Activity) context).showDialog(TIME_DIALOG_ID);
	}
	
	private void init(){
	    final Calendar c = Calendar.getInstance();
	    mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
   
        if (observer != null) updateDisplay();
	}
	
	public Dialog createTimePickerDialog(){
		return new TimePickerDialog(context, mTimeSetListener, mHour, mMinute, false);
	}
	
	public void prepareTimePickerDialog(Dialog dialog){
		((TimePickerDialog) dialog).updateTime(mHour, mMinute);
	}
	
	public void setObserver(TextView observer){
		this.observer = observer;
	}
	
	private void updateDisplay() {
		observerText = new StringBuilder()
        // Month is 0 based so add 1
		.append(pad(mHour)).append(":")
        .append(pad(mMinute));
        observer.setText(observerText);
    }

        
    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {

                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    mHour = hourOfDay;
                    mMinute = minute;
                    updateDisplay();
                }
            };
    
    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

}
