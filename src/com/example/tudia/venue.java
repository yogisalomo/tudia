package com.example.tudia;

public class venue extends place {
	private int category;
	public venue(int _id, String _name, String _description, String _address,
			int _minBudget, int _maxBudget, long _latitude, long _longitude, int _category) {
		super(_id, _name, _description, _address, _minBudget, _maxBudget,
				_latitude, _longitude);
		// TODO Auto-generated constructor stub
		category = _category;
	}
	public String getCategory(){
		if(category== 1){
			return "Outdoor";
		}
		else if(category== 2){
			return "Education";
		}
		else if(category == 3){
			return "Culture";
		}
		else if(category== 4){
			return "Shopping";
		}
		else{
			return "Dummy";
		}
	}
}
